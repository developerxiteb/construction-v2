<?php
/*
Plugin Name: Xiteb Contact Form Plugin
Plugin URI: http://xiteb.com
Description: Xiteb
Version: 1.0
Author: Aroos De khan
Author URI: http://w3guy.com
*/

function html_form_code() {

    echo '<form action="' . esc_url( $_SERVER['REQUEST_URI'] ) . '" method="post">';
    ?>
     <div class='row'>
<div class='col-lg-6 col-md-6 new-nb contact-input'>

    <div class='input-group contact-input mb-3'>
        <div class='input-group-prepend'>
            <span class='input-group-text'><i class='fa fa-user-o' aria-hidden='true'></i>
            </span>
        </div>
        <input type='text' class='form-control' name="username" placeholder='Name' aria-label='Username' Required>
    </div>

</div>
<div class='col-lg-6 col-md-6 new-nb'>
 
        <div class='input-group contact-input mb-3'>
            <div class='input-group-prepend'>
                <span class='input-group-text'><i class='fa fa-envelope-o' aria-hidden='true'></i>
                </span>
            </div>
            <input type='email' class='form-control' name="email_address" placeholder='Email Address' aria-label='Username' Required>
        </div>
   
</div>
<div class='col-lg-6 col-md-6 new-nb'>
   
        <div class='input-group contact-input mb-3'>
            <div class='input-group-prepend'>
                <span class='input-group-text'><i class='fa fa-phone' aria-hidden='true'></i>
                </span>
            </div>
            <input type='text' class='form-control' name="phone_number" placeholder='Phone Number' aria-label='Username' Required>
        </div>
  
</div>
<div class='col-lg-6 col-md-6 new-nb'>
    
        <div class='input-group contact-input mb-3'>
            <div class='input-group-prepend'>
                <span class='input-group-text'><i class='fa fa-map-marker' aria-hidden='true'></i>
                </span>
            </div>
            <input type='text' class='form-control' name="location" placeholder='Location' aria-label='Username' Required>
        </div>
   
</div>
<div class='col-lg-12'>
        <div class='form-group'>
            <textarea class='form-control mgs' name="message" id='exampleFormControlTextarea1' placeholder='Message'
                rows='3' Required></textarea>
        </div>
        <div class='con-btn-main text-center'>
     <input type='submit' value='Submit Now' class='con-btn' name="cf-submitted">
        </div>
</div>
</div>
<?php
    echo '</form>';
}

function deliver_mail() {

    // if the submit button is clicked, send the email
    if ( isset( $_POST['cf-submitted'] ) ) {

        // sanitize form values
        $username    = sanitize_text_field( $_POST['username'] );
        $email   = sanitize_email( $_POST['email_address'] );
        $phone_number    = sanitize_text_field( $_POST['phone_number'] );
        $location    = sanitize_text_field( $_POST['location'] );
        $ct_message = esc_textarea( $_POST['message'] );

        // get the blog administrator's email address
		$to = get_option( 'admin_email' );
        $message= "Email:".$email."<br/>Phone :".$phone_number."<br/> Location:". $location."<br/>Message :".$ct_message;
		$headers = "From: $username <$email>" . "\r\n";

		// If email has been process for sending, display a success message
		if (wp_mail( $to, $location, $message, $headers ) ) {
			echo '<div>';
			echo '<p>Thanks for contacting me, expect a response soon.</p>';
			echo '</div>';
		} else {
			echo 'An unexpected error occurred';
		}
	}
}

function cf_shortcode() {
	ob_start();
	deliver_mail();
	html_form_code();

	return ob_get_clean();
}

add_shortcode( 'sitepoint_contact_form', 'cf_shortcode' );

        ?>